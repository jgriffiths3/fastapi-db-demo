"""Unit tests for myapi."""

import pytest
import uuid


def test_create_user(client):
    """Test creating users at '/users/'.
    """
    response = client.get("/users/")
    assert response.status_code == 200
    assert response.json() == []

    response = client.post("/users/", json={
        "name": "Джошуа",
        "email": "joshua@example.com",
    })
    assert response.status_code == 200
    # Will raise an exception if it's clearly not a UUID.
    uid = uuid.UUID(response.json()['uid'])

    assert response.json()['name'] == "Джошуа"
    assert response.json()['email'] == "joshua@example.com"


def test_create_user_account(client):
    """Test creating user accounts at '/users/<id>/account/'
    """
    response = client.post("/users/", json={
        "name": "Джошуа",
        "email": "joshua2@example.com",
    })
    assert response.status_code == 200

    user_id = response.json()['uid']
    response = client.post(f'/users/{user_id}/accounts/', json={
        "name": "Joshs Account",
    })
    assert response.status_code == 200
    # Will raise an exception if it's clearly not a UUID.
    uid = uuid.UUID(response.json()['uid'])
    assert response.json()['name'] == "Joshs Account"

"""Unit-test fixtures/setup."""

import pytest

from fastapi.testclient import TestClient
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

import myapi.app
import myapi.db


@pytest.fixture
def client():
    """Get a TestClient for FastAPI"""
    with TestClient(myapi.app.API) as client:
        yield client


@pytest.fixture(scope="session", autouse=True)
def set_up_db():
    """Override the 'db' FastAPI dependency to use an in-memory SQLite DB."""
    uri = 'sqlite://'  # In-memory.
    engine = create_engine(uri, connect_args={'check_same_thread': False})
    TestSessionLocal = sessionmaker(autocommit=False,
                                    autoflush=True,
                                    bind=engine)

    def db_override():
        """Get a new DB session and create all defined tables. """
        db = TestSessionLocal()
        myapi.db.Base.metadata.create_all(bind=engine)
        try:
            yield db
        finally:
            db.close()

    myapi.app.API.dependency_overrides[myapi.app.db] = db_override

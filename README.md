# MyAPI

* Requires Python >= 3.7

## Dependencies

```
pip install -r requirements.txt
```

## Running

To run the server...
```
export MYAPI_DB=/path/to/my.db
uvicorn myapi.app:API
```

## Unit Tests

To run the unit-tests...

```
python -m pytest tests/
```

## Functional Tests

The functional (BDD) tests use behave.

NOTE: Currently the tests are hardcoded to connect to the API server at http://127.0.0.1:8000

1. Start the server:
```
export MYAPI_DB=/tmp/myapitest.db
rm -f "$MYAPI_DB"  # Make sure we have a clean DB!
uvicorn myapi.app:API
```

2. Run the tests:
```
behave
```

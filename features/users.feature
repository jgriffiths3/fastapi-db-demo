Feature: API to create a new User
    As an Account administrator, I want to create a new User.

    Scenario: Create a new user
        Given a request url http://127.0.0.1:8000/users/
            And a request json payload
                """
                {
                    "name": "Joshua Griffiths",
                    "email": "josh@example.com"
                }
                """
        When the request sends POST
        Then the response status is OK
            And the response json at $.name is equal to "Joshua Griffiths"

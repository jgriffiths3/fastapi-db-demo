"""FastAPI app entry-point.

Contains definitions for the various HTTP endpoints.
"""

from typing import List

from fastapi import Depends, FastAPI, HTTPException
from sqlalchemy.orm import Session

from . import crud, dbmodels, schemas
from .db import Base, ENGINE, SessionLocal


API = FastAPI()

# TODO(JG): Putting this here doesn't seem right.
Base.metadata.create_all(bind=ENGINE)


def db():
    """Get a new DB session.

    Invoked as a dependency to get a discrete session per-request.
    """
    session = SessionLocal()
    try:
        yield session
    finally:
        session.close()


@API.get("/users/{user_id}", response_model=schemas.User)
def read_user(user_id: str, db: Session = Depends(db)):
    db_user = crud.get_user(db, user_id)
    if db_user is None:
        raise HTTPException(404, f"No user exists with ID: {user_id}")

    return db_user


@API.get("/users/", response_model=List[schemas.User])
def read_users(db: Session = Depends(db)):
    return crud.get_users(db)


@API.post("/users/", response_model=schemas.User)
def create_user(user: schemas.UserCreate, db: Session = Depends(db)):
    existing_user = crud.get_user_by_email(db, user.email)
    if existing_user is not None:
        raise HTTPException(400, ("User already exists with email: "
                                  f"{user.email}"))

    return crud.create_user(db, user)


@API.put("/users/{user_id}", response_model=schemas.User)
def update_user(user_id: str,
                user: schemas.UserCreate,
                db: Session = Depends(db)):
    return crud.update_user(db, user_id, user)


@API.get("/users/{user_id}/accounts/{account_id}",
         response_model=schemas.Account)
def read_account(user_id: str, account_id: str, db: Session = Depends(db)):
    db_acc = crud.get_user_account(db, account_id, user_id)
    if db_acc is None:
        raise HTTPException(404, (f"No account with ID {account_id} exists for "
                                  f"user with ID: {user_id}"))

    return db_acc


@API.get("/users/{user_id}/accounts/", response_model=List[schemas.Account])
def read_accounts(user_id: str, db: Session = Depends(db)):
    if crud.get_user(db, user_id) is None:
        raise HTTPException(404, f"No user exists with ID: {user_id}")

    return crud.get_user_accounts(db, user_id)


@API.post("/users/{user_id}/accounts/", response_model=schemas.Account)
def create_account(user_id: str,
                   account: schemas.AccountCreate,
                   db: Session = Depends(db)):
    return crud.create_user_account(db, account, user_id)

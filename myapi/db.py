"""Base SQLAlchemy engine/session definitions."""

import os
import os.path

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

DB_PATH = os.environ.get('MYAPI_DB', 'myapi.db')
DB_URI = 'sqlite:///{}'.format(os.path.abspath(DB_PATH))
# Disabled as-per FastAPI docs; each request gets its own session.
_CONNECT_ARGS = {"check_same_thread": False}
ENGINE = create_engine(DB_URI, connect_args=_CONNECT_ARGS)

SessionLocal = sessionmaker(autocommit=False, autoflush=True, bind=ENGINE)
Base = declarative_base()

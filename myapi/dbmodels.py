"""ORM table/row models.

Definitions for ORM types.
"""

from sqlalchemy import Integer, Column, String, ForeignKey
from sqlalchemy.orm import relationship

import myapi.db


class User(myapi.db.Base):

    __tablename__ = 'users'

    uid = Column(String, primary_key=True, index=True)
    email = Column(String, unique=True, index=True)
    name = Column(String)
    accounts = relationship("Account", back_populates="user")


class Account(myapi.db.Base):

    __tablename__ = 'accounts'

    uid = Column(String, primary_key=True, index=True)
    name = Column(String)
    user_id = Column(Integer, ForeignKey("users.uid"))
    user = relationship("User", back_populates="accounts")

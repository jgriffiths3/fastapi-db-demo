"""Functions for performing specific CRUD operations on the DB."""

import uuid

from sqlalchemy.orm import Session

from . import db, dbmodels, schemas


def get_user(db: Session, user_id: str):
    """Get one user by ID."""
    return db.query(dbmodels.User).filter(dbmodels.User.uid == user_id).first()


def get_user_by_email(db: Session, email: str):
    """Get one user by ID."""
    return db.query(dbmodels.User).filter(dbmodels.User.email == email).first()


def get_users(db: Session):
    """Get all users."""
    return db.query(dbmodels.User).all()


def create_user(db: Session, user: schemas.UserCreate):
    """Create one user."""
    uid = str(uuid.uuid4())
    db_user = dbmodels.User(uid=uid,
                            name=user.name,
                            email=user.email)
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user


def update_user(db: Session, user_id: str, user: schemas.UserCreate):
    """Update one user."""
    db.query(dbmodels.User).filter(
        dbmodels.User.uid == user_id).update(user.dict())

    db.commit()
    return get_user(db, user_id)


def get_user_account(db: Session, account_id: str, user_id: str):
    """Create one account associated with one user."""
    return db.query(dbmodels.Account).filter(
        dbmodels.Account.user_id == user_id and
        dbmodels.Account.uid == account_id
    ).first()


def get_user_accounts(db: Session, user_id: str):
    """Create all accounts associated with one user."""
    return db.query(dbmodels.Account).filter(
        dbmodels.Account.user_id == user_id
    ).all()


def create_user_account(db: Session,
                        account: schemas.AccountCreate,
                        user_id: str):
    """Create one account associated with one user."""
    uid = str(uuid.uuid4())
    db_acc = dbmodels.Account(**account.dict(), uid=uid, user_id=user_id)
    db.add(db_acc)
    db.commit()
    db.refresh(db_acc)
    return db_acc

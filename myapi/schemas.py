"""Schemas for API requests and responses."""

from typing import List
from typing import Optional

import pydantic


class AccountBase(pydantic.BaseModel):
    name: str


class AccountCreate(AccountBase):
    pass


class Account(AccountBase):
    uid: str
    user_id: str

    class Config:
        orm_mode = True


class UserBase(pydantic.BaseModel):
    name: str
    email: str


class UserCreate(UserBase):
    pass


class User(UserBase):
    uid: str
    accounts: List[Account] = []

    class Config:
        orm_mode = True
